import simpleGit from 'simple-git';
import { isValid } from 'date-fns';

export default {
  ssr: true,
  /*
   ** Nuxt target
   ** See https://nuxtjs.org/api/configuration-target
   */
  target: 'static',
  /*
   ** Headers of the page
   ** See https://nuxtjs.org/api/configuration-head
   */
  head: {
    title: "AUA",
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || '',
      },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },
  /*
   ** Global CSS
   */
  css: [],
  /*
   ** Plugins to load before mounting the App
   ** https://nuxtjs.org/guide/plugins
   */
  plugins: [
    '~/plugins/directives.js'
  ],
  /*
   ** Auto import components
   ** See https://nuxtjs.org/api/configuration-components
   */
  components: true,
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: ['@nuxt/typescript-build', '@nuxtjs/tailwindcss', '@nuxtjs/color-mode'],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    // Doc: https://github.com/nuxt/content
    '@nuxt/content',
  ],
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {},
  /*
   ** Content module configuration
   ** See https://content.nuxtjs.org/configuration
   */
  content: {
    markdown: {
      prism: {
        theme: 'prism-themes/themes/prism-nord.css'
      }
    }
  },

  /*
   ** Build configuration
   ** See https://nuxtjs.org/api/configuration-build/
   */
  build: {
    devtools: false,
    postCss: {
      plugins: {
        'postcss-import': {},
        'postcss-apply': {},
        'tailwindcss': {},
        'autoprefixer': {},
      },
      preset: {
        // Change the postcss-preset-env settings
        autoprefixer: {
          grid: true
        }
      }
    }
  },

  generate: {
      /* This currently will have to be manually edited as the crawler does
      ** not seem to parse components for NuxtLink's and therefore is unable
      ** to follow the links contained within the Post component.
      */
      routes: ['/posts/sitesetup', '/posts/gitlab-runner-setup', '/posts/knock_rs-testing', '/posts/mfa-with-a-pebble', '/posts/knock_rs-benchmarking']
  },

  hooks: {

    'content:file:beforeInsert': async (document) => {

      if (document.extension === '.md') {
        const stats = require('reading-time')(document.text)

        document.stats = stats

        // Initialize simple-git
        const git = simpleGit(__dirname)

        // Get git metadata for files first commit and latest
        let gitCreatedAt = null;
        let gitUpdatedAt = null;
        let gitFileLog = ''
        gitFileLog = await git.log({ file: "content" + document.path + document.extension })

        if (gitFileLog.all.length !== 0 || gitFileLog.all !== undefined) {
          const gitLogFirstObject = gitFileLog.all.pop()
          if (gitLogFirstObject && gitLogFirstObject.total !== 0) {
            gitCreatedAt = new Date(gitLogFirstObject.date)
          }

          const gitLogLatestObject = gitFileLog.all.pop()
          if (gitLogLatestObject && gitLogLatestObject.total !== 0) {
            gitUpdatedAt = new Date(gitFileLog.latest.date)
          }
        }

        document.gitInfo = { gitCreatedAt, gitUpdatedAt }
        document.gitCreatedAt = isValid(gitCreatedAt) ? gitCreatedAt : undefined
        document.gitUpdatedAt = isValid(gitUpdatedAt) ? gitUpdatedAt : undefined
        document.gitFileLog = gitFileLog !== '' ? gitFileLog : undefined
      }
    }
  },
}
