---
title: Setting up this site
description: a simple setup of a Nuxtjs blog
---

My initial thoughts for a blog are to be simple and easy. Of course though, I want
to write the code myself, or somewhat at least. I had liked Vuejs in the past and with a 
recent discovery of Nuxtjs this undertaking of development and management seemed a 
little more attainable.

With a really basic implementation built, it was committed to a Gitlab repo at [blog](https://gitlab.com/andrewkreuzer/blog)

What is needed though is some build and deployment options. Netlify is a great 
option for static websites, and the starter option includes all that I'll need for the
time being.

The only thing needed is allowing Netlify access to your git host, in my case Gitlab.
Then netlify will pull changes and build a static site. Add a domain name using their
domain service or just point your domain's nameservice to Netlify's dns hostnames, wait
for dns to propagate and a Let's Encrypt cert will be added automatically.

Simple and easy. Without giving up control of the sites content. Oh and it's free,
besides the domain name.
