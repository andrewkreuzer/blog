---
title: Benchmarking knock_rs' packet parser
description: Using criterion to run iterative loop tests on the packet parsing used in knock_rs
---

I'm looking at refactoring the packet parsing and capture utilities within
knock\_rs but wanted a way to test the performance before I changed anything.
Turns out the parsing is definitely not a restriction on knock\_rs' performance.
110 ns to parse a packet... ya definitely not slowing anything down. 

### Criterion
Although rust now has the ability to run benchmarks within the std library,
criterion has some default features which make it hard to pass up. The ability
to get graph printouts of the benchmark, with comparison to the previous run, is
great for testing out changes to the code base.

This is the default printout that criterion provides, slightly modified to fit
with the styling of this site. Nonetheless this is great!


### UPDATE
The packet parser and low level packet capture libraries have been changed.
Knock\_rs no longer relies on pnet and instead uses [ rust pcap ]( https://github.com/ebfull/pcap ).

The data below has been updated and the time to parse a packet has been reduced
by more than half, from around a 110 ns to just 40-45 ns. The speed of parsing
wasn't an issue with pnet but it is a very heavy library, and this not only
improved performance, compile time is now much shorter. 

It also gave me the excuse to build a packet parsing library of my
[ own ]( https://gitlab.com/andrewkreuzer/packet_parser ). Which
was an interesting project. My implementation is much smaller with not nearly as
much support as the whole of pnet, but works for my use case and I'll continue
to add support for more protocols and packet types as needed.

<criterion-benchmarks>
<template>
<div class="body">
<div>
<div class="flex">
<image-component image-link="/criterion/pdf_small.svg" image-hover="/criterion/pdf.svg"></image-component>
<image-component image-link="/criterion/regression_small.svg" image-hover="/criterion/regression.svg"></image-component>
</div>
<h4>Additional Statistics:</h4>
<table>
<thead>
<tr>
<th></th>
<th title="0.95 confidence level" class="ci-bound text-nord-polarNight-400">Lower bound</th>
<th class="text-nord-polarNight-400 dark:text-snowStorm-100">Estimate</th>
<th title="0.95 confidence level" class="ci-bound text-nord-polarNight-400">Upper bound</th>
</tr>
</thead>
<tbody>
<tr>
<td>Slope</td>
<td class="ci-bound">43.315 ns</td>
<td>43.613 ns</td>
<td class="ci-bound">43.941 ns</td>
</tr>
<tr>
<td>R&#xb2;</td>
<td class="ci-bound">0.9235409</td>
<td>0.9279888</td>
<td class="ci-bound">0.9225954</td>
</tr>
<tr>
<td>Mean</td>
<td class="ci-bound">43.178 ns</td>
<td>43.457 ns</td>
<td class="ci-bound">43.761 ns</td>
</tr>
<tr>
<td title="Standard Deviation">Std. Dev.</td>
<td class="ci-bound">1.1127 ns</td>
<td>1.5129 ns</td>
<td class="ci-bound">1.9740 ns</td>
</tr>
<tr>
<td>Median</td>
<td class="ci-bound">42.975 ns</td>
<td>43.308 ns</td>
<td class="ci-bound">43.504 ns</td>
</tr>
<tr>
<td title="Median Absolute Deviation">MAD</td>
<td class="ci-bound">1.0662 ns</td>
<td>1.4026 ns</td>
<td class="ci-bound">1.6209 ns</td>
</tr>
</tbody>
</table>
</div>
<div class="additional_plots">
<h4>Additional Plots:</h4>
<ul>

<li>
<image-link name="Typical" image-hover="/criterion/typical.svg"></image-link>
</li>
<li>
<image-link name="Mean" image-hover="/criterion/mean.svg"></image-link>
</li>
<li>
<image-link name="Std. Dev." image-hover="/criterion/SD.svg"></image-link>
</li>
<li>
<image-link name="Median" image-hover="/criterion/median.svg"></image-link>
</li>
<li>
<image-link name="MAD" image-hover="/criterion/MAD.svg"></image-link>
</li>
<li>
<image-link name="Slope" image-hover="/criterion/slope.svg"></image-link>
</li>
</ul>
</div>
</section>
</div>
<section class="plots">
<h3>Change Since Previous Benchmark</h3>
<div class="relative">
<table width="100%">
<tbody>
<tr>
<td>
<image-component image-link="/criterion/relative_pdf_small.svg" image-hover="/criterion/both/pdf.svg"></image-component>
</a>
</td>
<td>
<image-component image-link="/criterion/relative_regression_small.svg" image-hover="/criterion/both/regression.svg"></image-component>
</a>
</td>
</tr>
</tbody>
</table>
</div>
</section>
<section class="stats">
<div class="additional_stats">
<h4>Additional Statistics:</h4>
<table>
<thead>
<tr>
<th></th>
<th title="0.95 confidence level" class="ci-bound text-nord-polarNight-400">Lower bound</th>
<th class="text-nord-polarNight-400">Estimate</th>
<th title="0.95 confidence level" class="ci-bound text-nord-polarNight-400">Upper bound</th>
<th></th>
</tr>
</thead>
<tbody>
<tr>
<td>Change in time</td>
<td class="ci-bound">-63.135%</td>
<td>-62.615%</td>
<td class="ci-bound">-62.126%</td>
<td>(p = 0.00 &lt; 0.05)</td>
</tr>
</tbody>
</table>
Performance has improved.
</div>
<div class="additional_plots">
<h4>Additional Plots:</h4>
<ul>

<li>
<image-link name="Change in mean" image-hover="/criterion/change/mean.svg"></image-link>
</li>
<li>
<image-link name="Change in median" image-hover="/criterion/change/median.svg"></image-link>
</li>
<li>
<image-link name="T-Test" image-hover="/criterion/change/t-test.svg"></image-link>
</li>
</ul>
</div>
</section>
<section class="explanation">
<h4>Understanding this report:</h4>
<p>The plot on the left shows the probability of the function taking a certain amount of time. The red
curve represents the saved measurements from the last time this benchmark was run, while the blue curve
shows the measurements from this run. The lines represent the mean time per iteration. Click on the
plot for a larger view.</p>
<p>The plot on the right shows the two regressions. Again, the red line represents the previous measurement
while the blue line shows the current measurement.</p>
<p>See <a href="https://bheisler.github.io/criterion.rs/book/user_guide/command_line_output.html#change">the
documentation</a> for more details on the additional statistics.</p>
</section>
</div>

</template>
</criterion-benchmarks>
