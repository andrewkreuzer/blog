const path = require('path')

module.exports = {
  darkMode: 'media',
  purge: {
    // Learn more on https://tailwindcss.com/docs/controlling-file-size/#removing-unused-css
    enabled: process.env.NODE_ENV === 'production',
    content: [
      'content/**/*.md',
      path.join(__dirname, 'components/**/*.vue'),
      path.join(__dirname, 'layouts/**/*.vue'),
      path.join(__dirname, 'pages/**/*.vue'),
      path.join(__dirname, 'plugins/**/*.js'),
      'nuxt.config.js'
    ],
  },
  theme: {
    extend: {

      typography: {
        DEFAULT: {
          css: {
            a: {
              color: '#81a1c1',
              '&:hover': {
                color: '#5e81ac',
              },
            },

            h1: {
              color: '#2e3440',
            },

            h2: {
              color: '#2e3440',
            },

            h3: {
              color: '#2e3440',
            },

            h4: {
              color: '#2e3440',
            },

            strong: {
              color: '#2e3440',
            },
          },
        },

        dark: {
          css: {
            color: '#d8dee9',

            a: {
              color: '#81a1c1',
              '&:hover': {
                color: '#88c0d0',
              },
            },

            h1: {
              color: '#d8dee9',
            },

            h2: {
              color: '#d8dee9',
            },

            h3: {
              color: '#d8dee9',
            },

            h4: {
              color: '#d8dee9',
            },

            strong: {
              color: '#81a1c1',
            },

            'pre[class*="language-"]': {
              background: '#3b4252',
            },

            code: {
              background: '#3b4252',
              color: '#d8dee9',
              'border-radius': '3px',
            },
            th: {
              color: '#d8dee9',
            }
          },
        },
      },

      colors: {
        nord: {
          polarNight: {
            100: '#4c566a',
            200: '#434c5e',
            300: '#3b4252',
            400: '#2e3440'
          },
          snowStorm: {
            100: '#eceff4',
            200: '#e5e9f0',
            300: '#d8dee9',
          },
          frost: {
            100: '#8fbcbb',
            200: '#88c0d0',
            300: '#81a1c1',
            400: '#5e81ac',
          },
          aurora: {
            red: '#bf616a',
            orange: '#d08770',
            yellow: '#ebcb8b',
            green: '#a3be8c',
            pink: '#b48ead',
          },
        },
      }
    },
  },

  variants: {
    extend: {
      typography: ['dark'],
    }
  },

  plugins: [
    require("@tailwindcss/typography"),
  ],
}
