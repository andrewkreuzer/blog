for func in ./netlify-functions/*; do
  if [ -d $func ] && [ $func != "./netlify-functions/build" ]; then
    funcname=$(basename $func)
    go get $func/...
    go build -o ./netlify-functions/build/$funcname \
      $func/main.go
  fi
done
