package main

import (
	"bytes"
	"context"
	"encoding/json"
	"log"
	"net/http"
	"os"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

type Body struct {
    Token    string `json:"token"`
    User string `json:"user"`
    Device string `json:"device"`
    Title string `json:"title"`
    Message string `json:"message"`
}

func handler(
  ctx context.Context,
  request events.APIGatewayProxyRequest,
) (*events.APIGatewayProxyResponse, error) {

  url := "https://api.pushover.net/1/messages.json"

  page_title := request.Body
  body := Body{
    Token:  os.Getenv("PUSHOVER_TOKEN"),
    User:  os.Getenv("PUSHOVER_USER"),
    Title:  "New Site Visit",
    Message: page_title,
  }

  jsonValue, _ := json.Marshal(body)
  res, err := http.Post(url, "application/json", bytes.NewBuffer(jsonValue))

  if err != nil {
    log.Fatal(err)
  }

  if res != nil {
    log.Printf(res.Status)
  }


  return nil, nil
}

func main() {
  lambda.Start(handler)
}
